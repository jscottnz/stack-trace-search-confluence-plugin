package org.greatscotts.atlassian.confluence.stacktrace;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceExtractor;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceMacro;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceExtractor.StackTraceMacroHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Document.class, ContentEntityObject.class})
public class StackTraceExtractorTest {

	private StackTraceExtractor extractor;
	
	private Document document;
	private ContentEntityObject contentEntityObject;
	@Mock
	private Space nonContentEntityObject;

	@Mock
	private XhtmlContent xhtmlContent;
	
	private StringBuffer body;
	@Mock
	private PageContext pageContext;
	
	@Mock
	private StackTraceMacroIndexer macroIndexer;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		document = PowerMockito.mock(Document.class);
		contentEntityObject = PowerMockito.mock(ContentEntityObject.class);
		
		body = new StringBuffer();
		
		when(contentEntityObject.toPageContext()).thenReturn(pageContext);
		
		extractor = new StackTraceExtractor(xhtmlContent, macroIndexer);
	}
	
	@Test
	public void extractorShouldOnlyOperateOnContentEntityObject() {
		// this would otherwise cause a Class Cast Exception
		extractor.addFields(document, null, nonContentEntityObject);
	}

	@Test
	public void extractorShouldGetBody() {
		extractor.addFields(document, null, contentEntityObject);

		verify(contentEntityObject).getBodyAsString();
	}
	
	@Test
	public void extractorShouldHandleMacroDefinitions() throws XhtmlException {
		extractor.addFields(document, body, contentEntityObject);
		
		verify(xhtmlContent).handleMacroDefinitions(anyString(), (ConversionContext) anyObject(), (StackTraceMacroHandler)anyObject());
		verify(macroIndexer).indexMacros(anyListOf(MacroDefinition.class), eq(document));
	}
	
	@Test
	public void stackTraceMacroHandlerShouldOnlyAddStackTraceMacros() {
		List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
		StackTraceMacroHandler handler = extractor.new StackTraceMacroHandler(macros);
		
		MacroDefinition otherMacro = new MacroDefinition("other", null, null, null);
		handler.handle(otherMacro);
		assertEquals(0, macros.size());
		
		MacroDefinition stacktraceMacro = new MacroDefinition(StackTraceMacro.macroName, null, null, null);
		handler.handle(stacktraceMacro);
		assertEquals(1, macros.size());
		
	}

}
