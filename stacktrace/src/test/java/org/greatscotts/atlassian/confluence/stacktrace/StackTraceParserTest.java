/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.*;

import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.Line;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;
import org.junit.Before;
import org.junit.Test;

public class StackTraceParserTest {

	private InputStream plainJavaTrace;
	private StackTraceParser parser;

	@Before
	public void setup() {
		parser = new StackTraceParser();
		plainJavaTrace = getClass().getResourceAsStream("/plain-java-trace.txt");
		assertNotNull(plainJavaTrace);
	}
	
	@Test
	public void parserShouldSplitTraceIntoLines() throws StackTraceParserException {
		assertEquals(36, parser.parse(new InputStreamReader(plainJavaTrace)).size());
	}
	
	@Test
	public void parserShouldExtractWithLineNumber() {
		assertWithLineNumber(parser.extractFromLine(" at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:233)"));
		
		// and with junk
		assertWithLineNumber(parser.extractFromLine("[INFO] 2013-12-13 5:55:02 - at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:233)"));
	}
	private void assertWithLineNumber(Line line) {
		assertEquals("org.apache.catalina.core.StandardWrapperValve.invoke", line.classMethod);
		assertEquals("StandardWrapperValve.java", line.file);
		assertEquals(233, (int)line.lineNumber);
	}
	
	@Test
	public void parserShouldExtractCauseAndMessage() {
		assertCauseAndMessage(parser.extractFromLine("org.apache.tomcat.dbcp.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'"));
		
		// and with junk
		assertCauseAndMessage(parser.extractFromLine("[INFO] 2013-12-13 5:55:02 org.apache.tomcat.dbcp.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'"));
		
		// and with Caused By
		// and with junk
		assertCauseAndMessage(parser.extractFromLine("[INFO] 2013-12-13 5:55:02 Caused by: org.apache.tomcat.dbcp.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'"));
		
		// and with Caused By
		assertCauseAndMessage(parser.extractFromLine("Caused by: org.apache.tomcat.dbcp.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'"));
		
	}
	private void assertCauseAndMessage(Line line) {
		assertEquals("org.apache.tomcat.dbcp.dbcp.SQLNestedException", line.cause);
		assertEquals("Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'", line.message);
	}
	
	@Test
	public void parserShouldNotExtractUninterestingLines() {
		assertNull(parser.extractFromLine("rubbush line"));
	}
}
