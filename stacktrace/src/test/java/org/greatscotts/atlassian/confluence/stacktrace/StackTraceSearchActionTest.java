/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.Term;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.core.ConfluenceActionSupport;


public class StackTraceSearchActionTest {

	private StackTraceSearchAction action;
	@Mock
	private StackTraceFieldFactory factory;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		action = new StackTraceSearchAction();
		action.setFactory(factory);
	}
	
	@Test
	public void defaultActionShouldBeInput() {
		assertEquals(ConfluenceActionSupport.INPUT, action.execute());
	}
	
	@Test
	public void submittedActionShouldBeSuccess() throws StackTraceParserException {
		action.setStacktrace("stacktrace");
		
		List<Term> terms = new ArrayList<Term>();
		terms.add(new Term(StackTraceFieldFactory.EXACT_MATCH, "a"));
		terms.add(new Term(StackTraceFieldFactory.SINGLE_LINE, "b"));
		terms.add(new Term(StackTraceFieldFactory.SINGLE_LINE, "c"));
		
		when(factory.getTerms((Reader)anyObject())).thenReturn(terms);
		
		assertEquals(ConfluenceActionSupport.SUCCESS, action.execute());
		verify(factory).getTerms((Reader)anyObject());
		assertEquals("stem%3Aa+OR+%28stslm%3Ab+stslm%3Ac+%29", action.getQuery());
	}
}
