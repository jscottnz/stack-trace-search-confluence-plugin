/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Document.class, ContentEntityObject.class})
public class StackTraceMacroIndexerTest {

	private StackTraceMacroIndexer indexer;
	private Document document;
	private List<MacroDefinition> macros;
	@Mock
	private StackTraceFieldFactory fieldFactory;

	@Before
	public void setup() throws StackTraceParserException {
		MockitoAnnotations.initMocks(this);
		
		document = PowerMockito.mock(Document.class);
		indexer = new StackTraceMacroIndexer(fieldFactory);
		
		macros = new ArrayList<MacroDefinition>();
		macros.add(new MacroDefinition(StackTraceMacro.macroName, new PlainTextMacroBody("body1"), null, null));
		macros.add(new MacroDefinition(StackTraceMacro.macroName, new PlainTextMacroBody("body2"), null, null));
		
		List<Field> fields = new ArrayList<Field>();
		fields.add(new Field("", new StringReader("")));
		fields.add(new Field("", new StringReader("")));
		fields.add(new Field("", new StringReader("")));
		when(fieldFactory.getFields((Reader) anyObject())).thenReturn(fields);
	}
	
	@Test(expected = StackTraceMacroRuntimeException.class)
	public void indexerShouldOnlyHandleStackTraceMacros() {
		macros.clear();
		macros.add(new MacroDefinition("other", null, null, null));
		indexer.indexMacros(macros, document);
	}
	
	@Test
	public void indexerShouldAddStacktraceFieldsToDocument() throws StackTraceParserException {
		indexer.indexMacros(macros, document);
		verify(fieldFactory, times(2)).getFields((Reader) anyObject());
		
		// 6 fields, 2 macros
		verify(document, times(6)).add((Fieldable) anyObject());
	}
	
	@Test 
	public void indexerShouldContinueOnParserError() throws StackTraceParserException {
		when(fieldFactory.getFields((Reader) anyObject())).thenThrow(new StackTraceParserException("EXPECTED TEST EXCEPTION", new Exception("EXPECTED TEST EXCEPTION")));
		indexer.indexMacros(macros, document);
	}
	
	
}
