/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.Line;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class StackTraceFieldFactoryTest {

	private StackTraceFieldFactory factory;
	@Mock
	private Reader reader;
	@Mock
	private StackTraceParser parser;
	private List<Line> lines;
	private Line cause1;

	@Before
	public void setup() throws StackTraceParserException {
		MockitoAnnotations.initMocks(this);

		lines = new ArrayList<StackTraceParser.Line>();

		cause1 = new Line("org.apache.tomcat.dbcp.dbcp.SQLNestedException",
		"Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'");
		
		lines.add(cause1);
		lines.add(new Line(
				"org.apache.tomcat.dbcp.dbcp.BasicDataSource.createDataSource",
				"BasicDataSource.java", 1136));
		lines.add(new Line(
				"org.apache.tomcat.dbcp.dbcp.BasicDataSource.getConnection",
				"BasicDataSource.java", 880));
		lines.add(new Line("java.lang.ClassNotFoundException",
				"COM.ibm.db2.jcc.DB2Driver"));
		lines.add(new Line("java.net.URLClassLoader$1.run", null));

		when(parser.parse(reader)).thenReturn(lines);
		factory = spy(new StackTraceFieldFactory(parser));
	}
	
	@Test
	public void getFieldsShouldGetSingleLinesField() throws StackTraceParserException {
		assertTrue(factory.getFields(reader).size() > 0);
		String testValue = factory.encodeLine(cause1);
		verify(factory).createIndexableField(StackTraceFieldFactory.SINGLE_LINE, testValue);
	}

	@Test
	public void getFieldsShouldGetExactField() throws StackTraceParserException {
		factory.getFields(reader);
		
		verify(factory).createIndexableField(StackTraceFieldFactory.EXACT_MATCH, "72abe");
	}
	
	@Test
	public void getTermsShouldGetSingleLinesField() throws StackTraceParserException {
		assertTrue(factory.getTerms(reader).size() > 0);
		String testValue = factory.encodeLine(cause1);
		verify(factory).createSearchTerm(StackTraceFieldFactory.SINGLE_LINE, testValue);
	}

	@Test
	public void getTermsShouldGetExactField() throws StackTraceParserException {
		factory.getTerms(reader);
		
		verify(factory).createSearchTerm(StackTraceFieldFactory.EXACT_MATCH, "72abe");
	}
	
	@Test
	public void encodeLineShouldProduceSomethingKnown() {
		assertEquals("86f76", factory.encodeLine(cause1));
	}
	
	@Test
	public void createIndexableField() {
		assertNotNull(factory.createIndexableField("a", "b"));
	}
	
	@Test
	public void createSearchTerm() {
		assertNotNull(factory.createSearchTerm("a", "b"));
	}
}
