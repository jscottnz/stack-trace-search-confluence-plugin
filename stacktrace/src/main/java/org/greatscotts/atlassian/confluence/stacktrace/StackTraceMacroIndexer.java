/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.StringReader;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.Line;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.xhtml.api.MacroDefinition;

public class StackTraceMacroIndexer {

	private static final Logger log = LoggerFactory
			.getLogger(StackTraceMacroIndexer.class);

	// how do we inject this?
	private final StackTraceFieldFactory fieldFactory;


	public StackTraceMacroIndexer(StackTraceFieldFactory fieldFactory) {
		this.fieldFactory = fieldFactory;
	}

	/**
	 * Adds a set of stacktrace macro fields into the lucene document
	 * @param macros
	 * @param document
	 */
	public void indexMacros(List<MacroDefinition> macros, Document document) {

		for (MacroDefinition macroDefinition : macros) {
			if (!macroDefinition.getName().equals(StackTraceMacro.macroName)) {
				throw new StackTraceMacroRuntimeException(
						"StackTraceMacroIndexer only accepts stacktrace macros");
			}

			try {
				List<Field> fields = fieldFactory.getFields(new StringReader(macroDefinition.getBodyText()));
				for (Field field : fields) {
					document.add(field);
				}
				
				
			} catch (StackTraceParserException e) {
				log.warn("{} failed to index stacktrace macro", e, getClass().getName());
				continue;
			}
			
			
		}

	}

}
