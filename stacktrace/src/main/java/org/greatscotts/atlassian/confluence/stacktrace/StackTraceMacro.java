/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;

public class StackTraceMacro implements com.atlassian.confluence.macro.Macro {

	public static final String macroName = "Stack Trace";

	@Override
	public String execute(Map<String, String> parameters, String body,
			ConversionContext context) throws MacroExecutionException {
		
		StringBuilder string = new StringBuilder();		
		string.append("<div class=\"code panel\" style=\"border-style: dashed;padding:0.5cm;border-radius: 15px\">");
		string.append("<div style=\"color: #DDDDDD\">Stack trace</div>");
		string.append("<pre>");
		string.append(body);
		string.append("</pre></div>");
		return string.toString();
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.PLAIN_TEXT;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

}
