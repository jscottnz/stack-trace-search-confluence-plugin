/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.bouncycastle.jce.provider.JCEMac.MD2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class StackTraceExtractor implements Extractor {

	private static final Logger log = LoggerFactory
			.getLogger(StackTraceExtractor.class);

	private final XhtmlContent xhtmlUtils;
	
	// how do we inject this?
	private final StackTraceMacroIndexer macroIndexer;

	public StackTraceExtractor(XhtmlContent xhtmlUtils) {
		// how do we inject this?
		this(xhtmlUtils, new StackTraceMacroIndexer(new StackTraceFieldFactory(new StackTraceParser())));
	}
	
	public StackTraceExtractor(XhtmlContent xhtmlUtils, StackTraceMacroIndexer macroIndexer) {
		this.xhtmlUtils = xhtmlUtils;
		this.macroIndexer = macroIndexer;
		log.debug("{} created", getClass().getName());
	}

	@Override
	public void addFields(Document document,
			StringBuffer defaultSearchableText, Searchable searchable) {

		log.debug("{} started", getClass().getName());

		// "fail" fast
		if (!(searchable instanceof ContentEntityObject)) {
			log.debug("{} is not canditate for indexing", searchable);
			return;
		}

		List<MacroDefinition> stackTraceMacros = new ArrayList<MacroDefinition>();
		ContentEntityObject contentEntityObject = (ContentEntityObject) searchable;
		ConversionContext context = new DefaultConversionContext(
				contentEntityObject.toPageContext(), "desktop");
		
		try {
			xhtmlUtils.handleMacroDefinitions(
					contentEntityObject.getBodyAsString(), context,
					new StackTraceMacroHandler(stackTraceMacros));
		} catch (XhtmlException e) {
			throw new StackTraceMacroRuntimeException(
					"Error while extracting macros", e);
		}
		
		macroIndexer.indexMacros(stackTraceMacros, document);

		log.debug("{} ended", getClass().getName());
	}

	class StackTraceMacroHandler implements MacroDefinitionHandler {
		private final List<MacroDefinition> macros;

		public StackTraceMacroHandler(List<MacroDefinition> macros) {
			this.macros = macros;
		}

		@Override
		public void handle(MacroDefinition macroDefinition) {

			if (macroDefinition.getName().equals(StackTraceMacro.macroName)) {
				macros.add(macroDefinition);
				log.debug("Adding StackTraceMacro");
			} else {
				log.debug("{} is not a candidate macro for indexing",
						macroDefinition.getName());
			}
		}
	}

}
