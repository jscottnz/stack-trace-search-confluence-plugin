/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.Term;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.Line;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.modzdetector.MD5HashAlgorithm;


/**
 * Encapsulates the logic for converting a stack trace into lucene fields.
 *
 */
public class StackTraceFieldFactory {
	
	private static final Logger log = LoggerFactory
	.getLogger(StackTraceFieldFactory.class);

	public static final String EXACT_MATCH = "stem";
	public static final String SINGLE_LINE = "stslm";
	private final StackTraceParser parser;

	public StackTraceFieldFactory(StackTraceParser parser) {
		this.parser = parser;
	}

	/**
	 * Given a stacktrace, returns Lucene fields to be indexed
	 * 
	 * @param Reader that contains the stacktace
	 * @return list of fields to index
	 * @throws StackTraceParserException
	 */
	public List<Field> getFields(Reader stacktraceReader) throws StackTraceParserException{
		
		final List<Field> fields = new ArrayList<Field>();
		parseStackTraceWithUsingStrategy(stacktraceReader, new FieldNameValueStrategy() {
			@Override
			public void perform(String name, String value) {
				fields.add(createIndexableField(name, value));
			}
		});
		
		return fields;
	}
	
	/**
	 * Given a stacktrace, returns Lucene fields to be searched
	 * 
	 * @param Reader that contains the stacktace
	 * @return list of terms to search
	 * @throws StackTraceParserException
	 */
	public List<Term> getTerms(Reader stacktraceReader) throws StackTraceParserException {
		final List<Term> terms = new ArrayList<Term>();
		parseStackTraceWithUsingStrategy(stacktraceReader, new FieldNameValueStrategy() {
			@Override
			public void perform(String name, String value) {
				terms.add(createSearchTerm(name, value));
			}
		});
		return terms;
	}

	/**
	 * Template method that parses a stack track and applies strategy to the interesting data 
	 * 
	 * @param stacktraceReader
	 * @param fieldNameStrategy
	 * @throws StackTraceParserException
	 */
	protected void parseStackTraceWithUsingStrategy(Reader stacktraceReader, FieldNameValueStrategy fieldNameStrategy) throws StackTraceParserException {
		List<Line> parse = parser.parse(stacktraceReader);
		StringBuilder exactMatchBuffer = new StringBuilder();
		for (Line line : parse) {
			 String encodedLine = encodeLine(line);
			 fieldNameStrategy.perform(SINGLE_LINE, encodedLine);
			 exactMatchBuffer.append(encodedLine);
		}
		
		fieldNameStrategy.perform(EXACT_MATCH, encodeLine(exactMatchBuffer.toString()));
	}
	
	protected interface FieldNameValueStrategy {
		void perform(String name, String value);
	}
	
	protected Field createIndexableField(String name, String value) {
		log.debug("Adding field {} value {}", name, value);
		return new Field(name, value, Store.YES, Index.NOT_ANALYZED_NO_NORMS);
	}
	
	protected Term createSearchTerm(String name, String value) {
		return new Term(name, value);
	}

	protected String encodeLine(Line line) {
		StringBuilder toEncode = new StringBuilder();
		toEncode.append(line.cause);
		toEncode.append(line.classMethod);
		toEncode.append(line.file);
		toEncode.append(line.message);
		
		return encodeLine(toEncode.toString());
	}
	
	private String encodeLine(String line) { 
		return new MD5HashAlgorithm().getHash(line.getBytes()).substring(0, 5);
	}

}
