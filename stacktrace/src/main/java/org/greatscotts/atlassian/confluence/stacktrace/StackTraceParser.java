/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StackTraceParser {

	/*
	 * Extracts class, filename and line number from the following:
	 * org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:233)
	 */
	private static Pattern classWithFileAndLineNumber = Pattern.compile("([^\\ ]+)\\(([^.]+\\.java):(\\d+)\\)$");
	
	/*
	 * Extracts the package and class from a line
	 * [INFO] 2013-12-13 5:55:02 Caused by: org.apache.tomcat.dbcp.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'
	 * first match = org.apache.tomcat.dbcp.dbcp.SQLNestedException
	 */
	private static Pattern packageAndClass = Pattern.compile("([a-zA-Z\\d_$.])+(?=\\.)([a-zA-Z\\d_$.])*");
	
	/*
	 * Extracts the cause message
	 * [INFO] 2013-12-13 5:55:02 Caused by: org.apache.tomcat.dbcp.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'
	 * first match = Cannot load JDBC driver class 'com.ibm.db2.jcc.DB2Driver'
	 */
	private static Pattern causeMessage = Pattern.compile("(?=\\.)[a-zA-Z\\d_$.]+:(.*)");

	public List<Line> parse(Reader inputReader) throws StackTraceParserException {
		
		List<Line> lines = new ArrayList<StackTraceParser.Line>();
		LineNumberReader reader = new LineNumberReader(inputReader);
		
		try {
			String line = reader.readLine();
			do {
					
					Line extractedLine = extractFromLine(line);
					if(extractedLine != null) {
						lines.add(extractedLine);
					}
					
					line = reader.readLine();
			} while (line != null);
		} catch (IOException e) {
			throw new StackTraceParserException("Exception while reading lines from stacetrace", e);
		}
		
		return lines;
		
	}
	
	/**
	 * Extracts a stack trace line broken down into class, filename and line number
	 * @param line from stack trace
	 * @return broken down line
	 */
	Line extractFromLine(String line) {
		
		Matcher classWithFileAndLineNumberMatcher = classWithFileAndLineNumber.matcher(line);
		Matcher packageAndClassMatcher = packageAndClass.matcher(line);
		if(classWithFileAndLineNumberMatcher.find()) {
			return new Line(classWithFileAndLineNumberMatcher.group(1), classWithFileAndLineNumberMatcher.group(2), Integer.parseInt(classWithFileAndLineNumberMatcher.group(3)));
		} else if(packageAndClassMatcher.find()) {
			String cause = packageAndClassMatcher.group(0).trim();
			String message = null;
			Matcher causeMessageMatcher = causeMessage.matcher(line);
			if(causeMessageMatcher.find()) {
				message = causeMessageMatcher.group(1).trim();
			}
			return new Line(cause, message);
		}
		
		return null;
	}

	/**
	 * A stack trace line broken down into is primitive components
	 *
	 */
	public static class Line {
		final Integer lineNumber;
		final String file;
		final String classMethod;
		final String cause;
		final String message;

		public Line(String cause, String message) {
			this.cause = cause;
			this.message = message;
			lineNumber = null;
			file = null;
			classMethod = null;
		}
		
		public Line(String clazz, String file, int lineNumber) {
			this.classMethod = clazz;
			this.file = file;
			this.lineNumber = lineNumber;
			this.cause = null;
			this.message = null;
		}
	}
	
	public static class StackTraceParserException extends Exception {
		public StackTraceParserException(String message, Throwable e) {
			super(message, e);
		}
	}

	

}
