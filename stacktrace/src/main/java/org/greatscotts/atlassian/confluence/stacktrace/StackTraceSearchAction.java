/*
Copyright 2013 Jeremy Scott

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.greatscotts.atlassian.confluence.stacktrace;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.lucene.index.Term;
import org.greatscotts.atlassian.confluence.stacktrace.StackTraceParser.StackTraceParserException;

import com.atlassian.confluence.core.ConfluenceActionSupport;

public class StackTraceSearchAction extends ConfluenceActionSupport {

	private String stacktrace;
	private String query;
	
	// how to we inject this?
	private StackTraceFieldFactory factory = new StackTraceFieldFactory(
			new StackTraceParser());

	public String execute() {
		if (stacktrace != null) {

			try {
				List<Term> terms = factory
						.getTerms(new StringReader(stacktrace));
				
				StringBuilder singleLines = new StringBuilder();
				String exactMatch = "";
				
				for (Term term : terms) {
					
					if(term.toString().startsWith(StackTraceFieldFactory.EXACT_MATCH)) {
						exactMatch = term.toString();
					} else {
						singleLines.append(term.toString());
						singleLines.append(" ");
					}
				}
				StringBuilder assembledQuery = new StringBuilder();
				assembledQuery.append(exactMatch);
				assembledQuery.append(" OR (");
				assembledQuery.append(singleLines);
				assembledQuery.append(")");
				
				query = URLEncoder.encode(   assembledQuery.toString().trim(), "UTF-8");
				
			} catch (StackTraceParserException e) {
				return handleError();
			} catch (UnsupportedEncodingException e) {
				return handleError();
			}
			return SUCCESS;
		}

		return INPUT;
	}

	public void setFactory(StackTraceFieldFactory factory) {
		this.factory = factory;
	}

	public void setStacktrace(String stacktrace) {
		this.stacktrace = stacktrace;
	}
	
	public String getQuery() {
		return query;
	}
	
	private String handleError() {
		return ERROR;
	}

}
